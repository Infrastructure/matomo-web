#!/bin/bash

tar cf - --one-file-system -C /usr/src/matomo . | tar xf -
cp -r /opt/matomo/extra-plugins/* /var/www/html/plugins/
php console core:archive
