#!/bin/bash

/usr/local/bin/geoipupdate -v
cp /usr/local/share/GeoIP/*.mmdb /var/www/html/misc

tar cf - --one-file-system -C /usr/src/matomo . | tar xf -
cp -r /opt/matomo/extra-plugins/* /var/www/html/plugins/
php /var/www/html/console core:update --yes

exec php-fpm
