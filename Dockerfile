FROM docker.io/library/matomo:5-fpm

USER root

RUN apt-get update && \
    apt-get install -y git unzip

RUN apt-get update && apt-get install -y nginx-light && \
    ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log
ADD nginx.conf /etc/nginx/nginx.conf

RUN apt-get install -y build-essential automake libtool autoconf && \
    git clone --recursive https://github.com/maxmind/libmaxminddb && \
    cd libmaxminddb && \
    autoreconf -fiv && ./configure && \
    make && make install && \
    cd .. && rm -rf libmaxminddb && \
    git clone --recursive https://github.com/maxmind/MaxMind-DB-Reader-php && \
    cd MaxMind-DB-Reader-php/ext && \
    phpize && \
    cp /usr/share/libtool/build-aux/config.guess . && \
    cp /usr/share/libtool/build-aux/config.sub . && \
    ./configure && make && make install && \
    docker-php-ext-enable maxminddb && \
    cd .. && rm -rf MaxMind-DB-Reader-php && \
    apt-get remove -y build-essential automake libtool autoconf && \
    apt-get autoremove -y

RUN curl -L https://github.com/maxmind/geoipupdate/releases/download/v7.0.1/geoipupdate_7.0.1_linux_arm64.tar.gz -o /tmp/geoipupdate.tgz && \
    cd /tmp && tar xf geoipupdate.tgz && \
    mv geoipupdate_*/geoipupdate /usr/local/bin/ && \
    rm -rf geoipupdate_* geoipupdate.tgz

RUN mkdir -p /opt/matomo/extra-plugins && \
    cd /opt/matomo/extra-plugins && \
    curl https://plugins.matomo.org/api/2.0/plugins/LoginOIDC/download/5.0.0 \
    -o LoginOIDC.zip && \
    unzip LoginOIDC.zip && \
    find LoginOIDC -type d | xargs chmod 775 && \
    find LoginOIDC -type f | xargs chmod 644 && \
    rm -f /opt/matomo/extra-plugins/LoginOIDC.zip /var/www/html/vendor/wikimedia/less.php/bin/lessc

RUN cd /opt/matomo/extra-plugins && \
    curl https://plugins.matomo.org/api/2.0/plugins/QueuedTracking/download/5.1.0 -o QueuedTracking.zip && \
    unzip QueuedTracking.zip && \
    find QueuedTracking -type d | xargs chmod 775 && \
    find QueuedTracking -type f | xargs chmod 644 && \
    rm -f /opt/matomo/extra-plugins/QueuedTracking.zip

RUN sed -i 's/pm.max_children =.*/pm.max_children = 15/' /usr/local/etc/php-fpm.d/www.conf

ADD start.sh /usr/local/bin/matomo-start
ADD archive.sh /usr/local/bin/matomo-archive
ADD queuedtracking.sh /usr/local/bin/matomo-queuedtracking

ENTRYPOINT ["/usr/local/bin/matomo-start"]
